describe('ventana principal', () => {
    it('Tiene encabezado correcto y en español por defecto', () => {
        cy.visit('http://localhost:4200');
        cy.contains('angular-wishlist');
        cy.get('h1 b').should('contain', 'HOLA es');
    });
    it('Puedo cambiar de idioma', () => {
        cy.get('div.page-header select').select('en');
        cy.get('h1 b').should('contain', 'HOLA en');
    });
    it('Puedo crear y eliminar tarjetas', () => {
        cy.get('app-destino-viaje div.card-desc h3:first-of-type').contains('@@@Dato de Prueba@@@').should('not.exist');
        cy.get('form[name=form] button[type=submit]').should('not.exist');
        cy.get('input#imagenUrl').type('@@@Imagen de Prueba@@@');
        cy.get('input#nombre').type('@@@Dato de Prueba@@@');
        cy.get('form[name=form] button[type=submit]').click();
        cy.get('app-destino-viaje').contains('@@@Dato de Prueba@@@', { timeout: 4000 }).should('have.length', 1).parentsUntil('app-destino-viaje').find('button.close>span').click();
        cy.get('app-destino-viaje div.card-desc h3:first-of-type').contains('@@@Dato de Prueba@@@').should('not.exist');
    });
});
