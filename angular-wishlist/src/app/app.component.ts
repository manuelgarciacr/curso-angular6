import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Store, select } from '@ngrx/store';
import { AppState } from './app.module';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent {
    title = 'angular-wishlist';
    time = new Observable((observer) => {
        setInterval(() => observer.next(new Date().toString()), 1000);
    });
    trackedClicks: string;

    constructor(
        public translate: TranslateService,
        private store: Store<AppState>
    ) {
        console.log('***************** get translation');
        translate
            .getTranslation('en')
            .subscribe((x) => console.log('x: ' + JSON.stringify(x)));
        translate.setDefaultLang('es');
        store
            .pipe(select((state: AppState) => state.destinos.trackedClicks))
            .subscribe((tc) => {
                this.trackedClicks = '';
                for (const tag in tc) {
                    if (this.trackedClicks === '')
                        this.trackedClicks = tag + ': ' + tc[tag];
                    else this.trackedClicks += '\n' + tag + ': ' + tc[tag];
                }
            });
    }
}
