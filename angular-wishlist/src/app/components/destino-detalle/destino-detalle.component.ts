import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { ActivatedRoute } from '@angular/router';
import { DestinosApiClient } from './../../models/destinos-api-client.model';

@Component({
    selector: 'app-destino-detalle',
    templateUrl: './destino-detalle.component.html',
    styleUrls: ['./destino-detalle.component.scss'],
    providers: [DestinosApiClient],
    styles: [
        `
            mgl-map {
                height: 50vh;
                width: 100vh;
            }
        `,
    ],
})
export class DestinoDetalleComponent implements OnInit {
    destino: DestinoViaje;
    style = {
        sources: {
            world: {
                type: 'geojson',
                data:
                    'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json',
            },
        },
        version: 8,
        layers: [
            {
                id: 'countries',
                type: 'fill',
                source: 'world',
                layout: {},
                paint: {
                    'fill-color': '#6F788A',
                },
            },
        ],
    };

    constructor(
        private route: ActivatedRoute,
        private api: DestinosApiClient
    ) {}

    ngOnInit(): void {
        const id = this.route.snapshot.paramMap.get('id');
        this.api.getById(id).then((res) => (this.destino = res));
    }
}
