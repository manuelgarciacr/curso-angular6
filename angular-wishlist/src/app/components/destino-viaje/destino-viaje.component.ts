import {
    Component,
    OnInit,
    Input,
    HostBinding,
    EventEmitter,
    Output,
} from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import {
    VoteUpAction,
    VoteDownAction,
} from '../../models/destinos-viajes-state.model';
import {
    trigger,
    state,
    style,
    transition,
    animate,
} from '@angular/animations';

@Component({
    selector: 'app-destino-viaje',
    templateUrl: './destino-viaje.component.html',
    styleUrls: ['./destino-viaje.component.scss'],
    animations: [
        trigger('esFavorito', [
            state(
                'estadoFavorito',
                style({
                    backgroundColor: 'PaleTurquoise',
                })
            ),
            state(
                'estadoNoFavorito',
                style({
                    backgroundColor: 'WhiteSmoke',
                })
            ),
            transition('estadoNoFavorito => estadoFavorito', [animate('3s')]),
            transition('estadoFavorito => estadoNoFavorito', [animate('1s')]),
        ]),
    ],
})
export class DestinoViajeComponent implements OnInit {
    @Input() destino: DestinoViaje;
    // tslint:disable-next-line: no-input-rename
    @Input('idx') position: number;
    @HostBinding('attr.class') cssClass = 'col-md-4';
    @Output() clicked: EventEmitter<DestinoViaje>;
    @Output() erase: EventEmitter<DestinoViaje>;

    constructor(private store: Store<AppState>) {
        this.erase = new EventEmitter();
        this.clicked = new EventEmitter();
    }

    ngOnInit(): void {}

    fn_erase(): boolean {
        this.erase.emit(this.destino);
        return false;
    }

    ir(): boolean {
        this.clicked.emit(this.destino);
        return false;
    }

    voteUp(): boolean {
        this.store.dispatch(new VoteUpAction(this.destino));
        return false;
    }

    voteDown(): boolean {
        this.store.dispatch(new VoteDownAction(this.destino));
        return false;
    }
}
