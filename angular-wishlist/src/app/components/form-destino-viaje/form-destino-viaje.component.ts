import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl,
    ValidatorFn,
} from '@angular/forms';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { fromEvent } from 'rxjs';
import {
    map,
    filter,
    debounceTime,
    distinctUntilChanged,
    switchMap,
} from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';

@Component({
    selector: 'app-form-destino-viaje',
    templateUrl: './form-destino-viaje.component.html',
    styleUrls: ['./form-destino-viaje.component.scss'],
})
export class FormDestinoViajeComponent implements OnInit {
    // tslint:disable-next-line: no-output-on-prefix
    @Output() onItemAdded: EventEmitter<DestinoViaje>;
    fg: FormGroup;
    minVal = 5;
    minLongitud = 3;
    searchResults: string[];

    constructor(fb: FormBuilder) {
        this.onItemAdded = new EventEmitter();
        this.fg = fb.group({
            nombre: [
                '',
                Validators.compose([
                    Validators.required,
                    this.nombreValidator.bind(this),
                    this.nombreValidatorParametrizable(this.minLongitud),
                ]),
            ],
            url: [''],
        });
        this.fg.valueChanges.subscribe((form: any) => {
            console.log('Cambio en el formulario', form);
        });
    }

    ngOnInit(): void {
        const elemNombre = document.getElementById('nombre') as HTMLInputElement;
        fromEvent(elemNombre, 'input')
            .pipe(
                map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
                filter((text) => text.length > 2),
                debounceTime(200),
                distinctUntilChanged(),
                switchMap(() => ajax('/assets/datos.json'))
            )
            .subscribe((ajaxResponse) => {
                this.searchResults = ajaxResponse.response.filter(x => {
                    return x.toLowerCase().includes(elemNombre.value.toLowerCase());
                });
            });
    }

    guardar(nombre: string, url: string): boolean {
        const d = new DestinoViaje(nombre, url);
        this.onItemAdded.emit(d);
        return false;
    }

    nombreValidator(control: FormControl): { [s: string]: boolean } {
        const l = control.value.toString().trim().length;
        if (l > 0 && l < this.minVal) return { invalidNombre: true };
        return null;
    }

    nombreValidatorParametrizable(min: number): ValidatorFn {
        return (control: FormControl): { [s: string]: boolean } | null => {
            const l = control.value.toString().trim().length;
            if (l > 0 && l < min) return { minLongNombre: true };
            return null;
        };
    }
}
