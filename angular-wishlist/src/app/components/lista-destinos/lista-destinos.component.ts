import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { Store, select } from '@ngrx/store';
import { AppState } from '../../app.module';

@Component({
    selector: 'app-lista-destinos',
    templateUrl: './lista-destinos.component.html',
    styleUrls: ['./lista-destinos.component.scss'],
    providers: [DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {
    updates: string[];
    all: DestinoViaje[];
    trackedClicks: number;

    constructor(public destinosApiClient: DestinosApiClient, private store: Store<AppState>) {
        this.updates = [];
        store.pipe(select((state: AppState) => state.destinos.favorito))
            .subscribe(d => {
                if (d != null)
                    this.updates.push(`Se ha elegido a ${d.nombre}`);
            });
        store.pipe(select(state => state.destinos.items)).subscribe(items => this.all = items);
    }

    ngOnInit(): void {}

    agregado(d: DestinoViaje): void {
        this.destinosApiClient.add(d);
    }

    elegido(d: DestinoViaje): void {
        this.destinosApiClient.elegir(d);
    }

    eliminar(d: DestinoViaje): void {
        this.destinosApiClient.remove(d);
    }
}
