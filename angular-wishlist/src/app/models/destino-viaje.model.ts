import { v4 as uuid } from 'uuid';

export class DestinoViaje {
    private selected: boolean;

    constructor(
        public nombre: string,
        public u: string,
        public votes: number = 0,
        public id: string = uuid(),
        public servicios: string[] = ['pileta', 'desayuno']
    ) {}

    isSelected(): boolean {
        return this.selected;
    }

    setSelected(s: boolean): void {
        this.selected = s;
    }

    voteUp(): void {
        this.votes++;
    }

    voteDown(): void {
        this.votes--;
    }
}
