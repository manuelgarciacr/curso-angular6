import { DestinoViaje } from './destino-viaje.model';
import {
    EliminarDestinoAction,
    ElegidoFavoritoAction,
    NuevoDestinoAction,
} from './../models/destinos-viajes-state.model';
import { Store, select } from '@ngrx/store';
import { AppState, APP_CONFIG, AppConfig, db } from '../app.module';
import { Injectable, Inject, forwardRef } from '@angular/core';
import { take } from 'rxjs/operators';
import {
    HttpRequest,
    HttpHeaders,
    HttpClient,
    HttpResponse,
} from '@angular/common/http';

@Injectable()
export class DestinosApiClient {
    constructor(
        private store: Store<AppState>,
        @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
        private http: HttpClient
    ) {}

    add(d: DestinoViaje): void {
        // this.store.dispatch(new NuevoDestinoAction(d));
        const headers: HttpHeaders = new HttpHeaders({
            'X-API-TOKEN': 'token-seguridad',
        });
        const req = new HttpRequest(
            'POST',
            this.config.apiEndpoint + '/my',
            { nuevo: d },
            { headers }
        );
        this.http.request(req).subscribe((data: HttpResponse<{}>) => {
            if (data.status === 200) {
                this.store.dispatch(new NuevoDestinoAction(d));
                const myDb = db;
                myDb.destinos.add(d);
                console.log('todos los destinos de la db!');
                myDb.destinos
                    .toArray()
                    .then((destinos) => console.log(destinos));
            }
        });
    }

    remove(d: DestinoViaje): void {
        // this.store.dispatch(new EliminarDestinoAction(d));
        const headers: HttpHeaders = new HttpHeaders({
            'X-API-TOKEN': 'token-seguridad',
        });
        const req = new HttpRequest(
            'DELETE',
            this.config.apiEndpoint + '/my',
            { delete: d.id },
            { headers }
        );
        this.http.request(req).subscribe((data: HttpResponse<{}>) => {
            if (data.status === 200 || data.status === 204) {
                this.store.dispatch(new EliminarDestinoAction(d));
                const myDb = db;
                myDb.destinos.where('id').equals(d.id).delete();
                console.log('todos los destinos de la db!');
                myDb.destinos
                    .toArray()
                    .then((destinos) => console.log(destinos));
            }
        });
    }

    elegir(d: DestinoViaje): void {
        this.store.dispatch(new ElegidoFavoritoAction(d));
    }

    async getById(id: string): Promise<DestinoViaje> {
        return await this.store
            .pipe(
                select(
                    (state) =>
                        state.destinos.items.filter((i) => i.id === id)[0]
                ),
                take(1)
            )
            .toPromise<DestinoViaje>();
    }
}
