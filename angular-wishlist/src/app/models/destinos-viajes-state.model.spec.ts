import {
    reducerDestinosViajes,
    DestinosViajesState,
    initializeDestinosViajesState,
    InitMyDataAction,
    NuevoDestinoAction,
    EliminarDestinoAction,
    ElegidoFavoritoAction,
    VoteUpAction,
    VoteDownAction,
    IncrementarTrackedClicksAction
} from './destinos-viajes-state.model';
import { DestinoViaje } from './destino-viaje.model';

describe('reducerDestinosViajes', () => {
    it('should reduce INIT_MY_DATA', () => {
        // setup
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const action: InitMyDataAction = new InitMyDataAction([
            new DestinoViaje('destino 1', 'uuidString'),
            new DestinoViaje('destino 2', 'uuidString'),
        ]);
        // action
        const newState: DestinosViajesState = reducerDestinosViajes(
            prevState,
            action
        );
        // assertions
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual('destino 1');
        expect(newState.favorito).toBeNull();
        expect(newState.loading).toEqual(false);
        // tear down. Deshacer acciones (BDD, etc.)
    });
    it('should reduce NUEVO_DESTINO', () => {
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const action: NuevoDestinoAction = new NuevoDestinoAction(
            new DestinoViaje('barcelona', 'url')
        );
        const newState = reducerDestinosViajes(prevState, action);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].nombre).toEqual('barcelona');
        expect(newState.favorito).toEqual(null);
        expect(newState.loading).toEqual(false);
    });
    it('should reduce ELIMINAR_DESTINO', () => {
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        prevState.items.push(new DestinoViaje('destino1', 'url'));
        prevState.loading = false;
        prevState.favorito = prevState.items[0];
        const action: EliminarDestinoAction = new EliminarDestinoAction(
            prevState.items[0]
        );
        const newState = reducerDestinosViajes(prevState, action);
        expect(newState.items.length).toEqual(0);
        expect(newState.favorito).toBeNull();
        expect(newState.loading).toEqual(false);
    });
    it('should reduce ELEGIDO_FAVORITO', () => {
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        prevState.items.push(
            new DestinoViaje('destino1', 'url1'),
            new DestinoViaje('destino1', 'url1')
        );
        prevState.loading = false;
        prevState.favorito = prevState.items[0];
        const action: ElegidoFavoritoAction = new ElegidoFavoritoAction(
            prevState.items[1]
        );
        const newState = reducerDestinosViajes(prevState, action);
        expect(newState.items.length).toEqual(2);
        expect(newState.favorito).toEqual(prevState.items[1]);
        expect(newState.loading).toEqual(false);
    });
    it('should reduce VOTE_UP', () => {
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        prevState.items.push(
            new DestinoViaje('destino1', 'url1'),
            new DestinoViaje('destino2', 'url2')
        );
        prevState.loading = false;
        prevState.favorito = prevState.items[1];
        const action: VoteUpAction = new VoteUpAction(prevState.items[0]);
        let newState = reducerDestinosViajes(prevState, action);
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].votes).toEqual(1);
        expect(newState.favorito).toEqual(prevState.items[1]);
        expect(newState.loading).toEqual(false);
        newState = reducerDestinosViajes(newState, action);
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].votes).toEqual(2);
        expect(newState.favorito).toEqual(prevState.items[1]);
        expect(newState.loading).toEqual(false);
    });
    it('should reduce VOTE_DOWN', () => {
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        prevState.items.push(
            new DestinoViaje('destino1', 'url1'),
            new DestinoViaje('destino2', 'url2')
        );
        prevState.items[0].votes = 1;
        prevState.loading = false;
        prevState.favorito = prevState.items[1];
        const action: VoteDownAction = new VoteDownAction(prevState.items[0]);
        let newState = reducerDestinosViajes(prevState, action);
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].votes).toEqual(0);
        expect(newState.favorito).toEqual(prevState.items[1]);
        expect(newState.loading).toEqual(false);
        newState = reducerDestinosViajes(newState, action);
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].votes).toEqual(-1);
        expect(newState.favorito).toEqual(prevState.items[1]);
        expect(newState.loading).toEqual(false);
    });
    it('should reduce INCREMENTAR_TRACKED_CLICKS', () => {
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        let action: IncrementarTrackedClicksAction = new IncrementarTrackedClicksAction('Etiqueta1');
        let newState = reducerDestinosViajes(prevState, action);
        expect(newState.items.length).toEqual(0);
        expect(newState.favorito).toBeNull();
        expect(newState.loading).toEqual(false);
        expect(Object.keys(newState.trackedClicks).length).toEqual(1);
        expect(newState.trackedClicks.Etiqueta1).toEqual(1);
        action = new IncrementarTrackedClicksAction('Etiqueta2');
        newState = reducerDestinosViajes(newState, action);
        expect(newState.items.length).toEqual(0);
        expect(newState.favorito).toBeNull();
        expect(newState.loading).toEqual(false);
        expect(Object.keys(newState.trackedClicks).length).toEqual(2);
        expect(newState.trackedClicks.Etiqueta1).toEqual(1);
        expect(newState.trackedClicks.Etiqueta2).toEqual(1);
        newState = reducerDestinosViajes(newState, action);
        expect(newState.items.length).toEqual(0);
        expect(newState.favorito).toBeNull();
        expect(newState.loading).toEqual(false);
        expect(Object.keys(newState.trackedClicks).length).toEqual(2);
        expect(newState.trackedClicks.Etiqueta1).toEqual(1);
        expect(newState.trackedClicks.Etiqueta2).toEqual(2);
    });
});
