import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DestinoViaje } from './destino-viaje.model';

// ESTADO
export interface DestinosViajesState {
    items: DestinoViaje[];
    loading: boolean;
    favorito: DestinoViaje;
    trackedClicks: any;
}

export function initializeDestinosViajesState(): DestinosViajesState {
    return {
        items: [],
        loading: false,
        favorito: null,
        trackedClicks: {}
    };
}

// ACCIONES
export enum DestinosViajesActionTypes {
    NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
    ELIMINAR_DESTINO = '[Destinos Viajes] Eliminar',
    ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
    VOTE_UP = '[Destinos Viajes] Vote Up',
    VOTE_DOWN = '[Destinos Viajes] Vote Down',
    INIT_MY_DATA = '[Destinos Viajes] Init My Data',
    INCREMENTAR_TRACKED_CLICKS = '[DestinosViajes] Incrementar Tracked Clicks'
}

export class NuevoDestinoAction implements Action {
    type = DestinosViajesActionTypes.NUEVO_DESTINO;
    constructor(public destino: DestinoViaje) {}
}

export class EliminarDestinoAction implements Action {
    type = DestinosViajesActionTypes.ELIMINAR_DESTINO;
    constructor(public destino: DestinoViaje) {}
}

export class ElegidoFavoritoAction implements Action {
    type = DestinosViajesActionTypes.ELEGIDO_FAVORITO;
    constructor(public destino: DestinoViaje) {}
}

export class VoteUpAction implements Action {
    type = DestinosViajesActionTypes.VOTE_UP;
    constructor(public destino: DestinoViaje) {}
}

export class VoteDownAction implements Action {
    type = DestinosViajesActionTypes.VOTE_DOWN;
    constructor(public destino: DestinoViaje) {}
}

export class InitMyDataAction implements Action {
    type = DestinosViajesActionTypes.INIT_MY_DATA;
    constructor(public destinos: DestinoViaje[]) {}
}

export class IncrementarTrackedClicksAction implements Action {
    type = DestinosViajesActionTypes.INCREMENTAR_TRACKED_CLICKS;
    constructor(public tag: string) {}
}

export type DestinosViajesActions =
    | NuevoDestinoAction
    | EliminarDestinoAction
    | ElegidoFavoritoAction
    | VoteUpAction
    | VoteDownAction
    | InitMyDataAction
    | IncrementarTrackedClicksAction;

// REDUCERS
export function reducerDestinosViajes(
    state: DestinosViajesState,
    action: DestinosViajesActions
): DestinosViajesState {
    switch (action.type) {
        case DestinosViajesActionTypes.INIT_MY_DATA: {
            const estado = initializeDestinosViajesState();
            const destinos: DestinoViaje[] = (action as InitMyDataAction).destinos;
            return {
                ...estado,
                items: destinos.map((d) => new DestinoViaje(d.nombre, d.u, d.votes, d.id, d.servicios)),
            };
        }
        case DestinosViajesActionTypes.NUEVO_DESTINO: {
            return {
                ...state,
                items: [...state.items, (action as NuevoDestinoAction).destino],
            };
        }
        case DestinosViajesActionTypes.ELIMINAR_DESTINO: {
            const destino: DestinoViaje = (action as EliminarDestinoAction).destino;
            const favorite = destino.isSelected;
            const array = [...state.items];
            const index = array.indexOf(destino);
            if (index > -1) array.splice(index, 1);
            else return { ...state };
            if (favorite) return { ...state, items: array, favorito: null };
            else return { ...state, items: array };
        }
        case DestinosViajesActionTypes.ELEGIDO_FAVORITO: {
            state.items.forEach((x) => x.setSelected(false));
            const fav: DestinoViaje = (action as ElegidoFavoritoAction).destino;
            fav.setSelected(true);
            return {
                ...state,
                favorito: fav,
            };
        }
        case DestinosViajesActionTypes.VOTE_UP: {
            const d: DestinoViaje = (action as VoteUpAction).destino;
            d.voteUp();
            return {
                ...state,
            };
        }
        case DestinosViajesActionTypes.VOTE_DOWN: {
            const d: DestinoViaje = (action as VoteUpAction).destino;
            d.voteDown();
            return {
                ...state,
            };
        }
        case DestinosViajesActionTypes.INCREMENTAR_TRACKED_CLICKS: {
            const tag: string = (action as IncrementarTrackedClicksAction).tag;
            let counter: number;
            if (state.trackedClicks[tag] > 0)
                counter = state.trackedClicks[tag] + 1;
            else
                counter = 1;
            const newTrackedClicks = {...state.trackedClicks};
            newTrackedClicks[tag] = counter;
            return {
                ...state,
                trackedClicks: newTrackedClicks
            };
        }
    }
}

// EFFECTS
@Injectable()
export class DestinosViajesEffects {
    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(DestinosViajesActionTypes.NUEVO_DESTINO),
        map(
            (action: NuevoDestinoAction) =>
                new ElegidoFavoritoAction(action.destino)
        )
    );

    constructor(private actions$: Actions) {}
}
