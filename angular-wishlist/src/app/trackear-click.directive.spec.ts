import { TrackearClickDirective } from './trackear-click.directive';
import {
    ElementRef,
    NO_ERRORS_SCHEMA,
    Injectable,
} from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { AppState } from './app.module';

@Injectable()
export class MockElementRef {
    nativeElement: {};
}

describe('TrackearClickDirective', () => {
    const initialState = {} as AppState;
    let component: TrackearClickDirective;
    let elRef: ElementRef;
    let store: Store<AppState>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                TrackearClickDirective,
                { provide: ElementRef, useClass: MockElementRef },
                provideMockStore({ initialState }),
            ],
            schemas: [NO_ERRORS_SCHEMA],
        });
        component = TestBed.inject(TrackearClickDirective);
        elRef = TestBed.inject(ElementRef);
        store = TestBed.inject(Store);
    });

    it('should create an instance', () => {
        expect(component).toBeTruthy();
    });
});
